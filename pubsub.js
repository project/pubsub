(function(Drupal, $) {

Drupal.pubsub = Drupal.pubsub || {};
Drupal.pubsub.subscribers = Drupal.pubsub.subscribers || {};

/**
 * Publish a message to subscribers.
 */
Drupal.pubsub.publish = function(message) {
  var subscribers = Drupal.pubsub.subscribers;
  if (subscribers[message]) {
    for (var x = 0, length = subscribers[message].length; x < length; x++) {
      subscribers[message][x].apply(this, Array.prototype.slice.call(arguments, 1));
    }
  }
};

/**
 * Subscribe to a message.
 */
Drupal.pubsub.subscribe = function(message, callback) {
  var subscribers = Drupal.pubsub.subscribers;
  subscribers[message] = subscribers[message] || [];
  subscribers[message].push(callback);
};

})(Drupal, jQuery);
