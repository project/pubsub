(function(Drupal, $) {

/**
 * Publish all messages that were published on the back-end.
 */
Drupal.behaviors.pubsub_js = function(context) {
  Drupal.settings.pubsub = Drupal.settings.pubsub || {};
  var messages = Drupal.settings.pubsub.messages || [];
  for (var x = 0, length = messages.length; x < length; x++) {
    Drupal.pubsub.publish.apply(this, ([messages[x].message]).concat(messages[x].args));
  }
};

})(Drupal, jQuery);
